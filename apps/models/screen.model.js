const mongoose = require('mongoose')

const ScreenSchema = mongoose.Schema({
    room: String,
    content: String,
    location: String,
    date: Date
})

module.exports = mongoose.model('Screen', ScreenSchema)