const mongoose = require('mongoose')

const UserSchema = mongoose.Schema({
    pseudo: String,
    password: String,
    right: String,
    first_name: String,
    last_name: Date,
    email: String,
    token: Object
})

module.exports = mongoose.model('Users', UserSchema)