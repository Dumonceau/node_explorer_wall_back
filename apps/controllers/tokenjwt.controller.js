const nJwt = require('njwt');
const secureRandom = require('secure-random');
const signingKey = secureRandom(256, {type: 'Buffer'})
const claims = {
    iss: 'wall',
    sub: 'test',
    scope: 'self'
}

// exports.createToken = (duration = 3600000) => {
exports.createToken = (req, res)  => {
    let duration = 3600000;
    let token = nJwt.create(claims, signingKey);
    token.setExpiration(duration)
    console.log(token.compact())
    res.send(token)
    return token;
}

exports.verifyToken = (token = null) => {
    nJwt.verify(token, signingKey, (err, verifiedJwt) => {
      if(err){
        console.error('err token expired')
        reject(err)
      } else {
        console.log('this token is verified :' +verifiedJwt) // show the token
        resolve(true)
      }
    })
}