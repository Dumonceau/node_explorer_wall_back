module.exports = (app) => {
    const connection = require('../controllers/connection.controller.js')
    app.post('/register', connection.register)
    app.post('/signup', connection.signup)
}