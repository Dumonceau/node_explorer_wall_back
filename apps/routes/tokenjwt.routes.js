module.exports = (app) => {
    const tokenjwt = require('../controllers/tokenjwt.controller.js')
    app.get('/genjwt', tokenjwt.createToken)
}