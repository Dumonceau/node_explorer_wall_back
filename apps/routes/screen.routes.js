module.exports = (app) => {
    const screen = require('../controllers/screen.controller.js')
    app.get('/salle2', screen.findRoom)
    app.post('/write', screen.writeScreen)
}