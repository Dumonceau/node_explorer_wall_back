const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const jwt = require('njwt')
// Config files
const dbConfig = require('./config/database.conf')
const config = require('./config/server.conf')
console.log(config.host)
console.log(config.port)
console.log(dbConfig.url)
console.log(config.frontaddr)
app.get('/', (req, res) => {
    res.send('Hello, world!');
});
// parse request application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse request application/json
app.use(bodyParser.json({limit: '50mb'}))
app.use(cors({
    origin: config.frontaddr
}));
require ('./apps/routes/connection.routes')(app)
require ('./apps/routes/screen.routes')(app)
require ('./apps/routes/tokenjwt.routes')(app)
app.get('/test', (req, res) => {
    res.send({
        'test': 'foo'
    })
})
const Img = mongoose.model('Img', { file: String });
app.get('/get', (req, res) => {
    Img.find().then(response => {
        if (response) {
            res.json(response)
        }
    })
})
app.listen(config.port || 3000, () => {
    console.log(`Server running on port ${config.port || 3000}`);
});

mongoose.connect(dbConfig.url)
    .then(() => {
        console.log('Connected to the database')
        
        
    }).catch(err => {
        console.log('Cannot connect to the database. Exiting now...')
        process.exit()
    })
